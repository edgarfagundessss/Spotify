package com.java.Spotify.Controllers;

import com.java.Spotify.Entities.Song;
import com.java.Spotify.Services.SongService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/songs")
public class SongController {

    @Autowired
    SongService songService;

    @GetMapping
    public ResponseEntity<Page<Song>> listAllSongs(Pageable pageable){
        Page<Song> listSongs = songService.findAll(pageable);
        if (listSongs.hasContent()){
            return new ResponseEntity<Page<Song>>(listSongs, HttpStatus.FOUND);
        }else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("/songNameAsc")
    public ResponseEntity<List<Song>>tSongNameDesc (Pageable pageable){
        List<Song> listNameAsc = songService.findbyNameDesc(pageable);
        if (listNameAsc.equals(true)){
            return new ResponseEntity<List<Song>>(listNameAsc, HttpStatus.FOUND);
        }else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("/songNameAsc")
    public ResponseEntity<List<Song>>tSongNameAsc (Pageable pageable){
            List<Song> listNameAsc = songService.findbyNameAsc(pageable);
        if (listNameAsc.equals(true)){
            return new ResponseEntity<List<Song>>(listNameAsc, HttpStatus.FOUND);
        }else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
}
