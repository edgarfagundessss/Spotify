package com.java.Spotify.Services;

import com.java.Spotify.Entities.Song;
import com.java.Spotify.Repositories.SongRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SongService {

    @Autowired
    SongRepository songRepository;

    public Page<Song> findAll(Pageable pageable) {
        return songRepository.findAll(pageable);
    }

    public List<Song> findbyNameAsc(Pageable pageable) {
        return songRepository.findAll(Sort.by(Sort.Direction.ASC, "name"));
    }

    public List<Song> findbyNameDesc(Pageable pageable) {
        return songRepository.findAll(Sort.by(Sort.Direction.DESC, "name"));
    }

    }
